(in-package :marvin-web)

(defwidget footer-widget ()
  ((parent :initarg :parent
	   :reader parent)
   (height :initarg :height
	   :initform "2em"
	   :reader widget-height)))

(defmethod get-css-classes ((widget footer-widget))
  (append '(:navbar :navbar-dark :bg-dark :fixed-bottom :flex-md-nowrap :p-0 :shadow)
	  (call-next-method)))

(defmethod render ((widget footer-widget))
  (with-html
    (:div :class "container d-flex justify-content-between"
	  (:h6 :class "p-0 my-0 text-light"
	       (:small "Proudly made with Common Lisp"))
	  (:h6 :class "p-0 my-0 text-light"
	       (:small :id "marvin-version")))
    (:script (ps (http-request "api/v1/version"
			       nil
			       (lambda (data status)
				 (let ((text (if (eq 200 status)
						 (+ "Version " (@ data version 0) "." (@ data version 1) "." (@ data version 2))
						 "Version: N/A")))
				   (setf (chain document (get-element-by-id "marvin-version") text-content)
					 text))
				 nil))))))


(in-package :marvin-web)

(defparameter *collected-pages* nil)

(defwidget corpus-widget ()
  ((parent :initarg :parent
	   :reader parent)
   (menu-widget :initarg :menu
		:reader menu-widget)
   (content-widget :initarg :content
		   :reader content-widget)
   (platform-pages :initarg :platform-pages
	    :accessor platform-pages
	    :initform nil)
   (plugin-pages :initarg :plugin-pages
	    :accessor plugin-pages
	    :initform nil)))

(defmethod initialize-instance :after ((this corpus-widget) &key)
  (load-pages this)
  (setf (slot-value this 'content-widget) (make-instance 'content-widget :parent this))
  (setf (slot-value this 'menu-widget) (make-instance 'menu-widget :parent this)))

(defwidget page ()
  ((parent :initarg :parent
	   :reader parent)
   (name :initarg :name
	 :reader page-name)
   (tag :initarg :tag
	:reader route-tag)
   (selected :initarg :selected
	     :initform nil
	     :accessor page-selected)))

(defgeneric before-leaving (page)
  (:documentation "Called right before changing to another pane. Returning *NIL* causes the switch to be interrupted."))

(defmethod before-leaving ((page t))
  t)

(defun selected-page (corpus)
  (or (car (member t (platform-pages corpus) :key #'page-selected))
      (car (member t (plugin-pages corpus) :key #'page-selected))))

(defun switch-to-page (corpus page)
  (let ((current-page (selected-page corpus)))
    (unless (before-leaving current-page)
      (return-from switch-to-page nil))

    (setf (page-selected current-page) nil)
    
    (assert (null (selected-page corpus)))
    
    (setf (page-selected page) t)
    (update (content-widget corpus))
    (return-from switch-to-page t)))

(defun add-page (page)
  (push page *collected-pages*))

(defun load-pages (corpus)
  (setf *collected-pages* nil)
  (unless (every #'identity
		 (iterate:iterate
		   (iterate:for plugin-file in (directory (make-pathname :name :wild
									 :type "lisp"
									 :directory (list :absolute
											  (namestring (asdf:system-source-directory :marvin-web))
											  "pages" "platform"))))
		   (iterate:collect (load plugin-file))))
    (log:warn "Could not load all files for the Platform Pages."))

  (setf (platform-pages corpus) (reverse *collected-pages*))
  (when (platform-pages corpus)
    (setf (page-selected (car (platform-pages corpus))) t))
  
  (setf *collected-pages* nil)

  (unless (every #'identity
		 (iterate:iterate
		   (iterate:for plugin-file in (directory (make-pathname :name :wild
									 :type "lisp"
									 :directory (list :absolute
											  (namestring (asdf:system-source-directory :marvin-web))
											  "pages" "plugins"))))
		   (iterate:collect (load plugin-file))))
    (log:warn "Could not load all files for the Plugin Pages."))

  (setf (plugin-pages corpus) (reverse *collected-pages*))
  (setf *collected-pages* nil)

  (return-from load-pages))

(defmethod get-css-classes ((widget corpus-widget))
  (append '(:container-fluid)
	  (call-next-method)))

(defmethod render ((corpus corpus-widget))
  (with-html
    ;;(:div :class "container.fluid"
	  (:div :class "row"
		(render (menu-widget corpus))
		(render (content-widget corpus)))));;)


(defmethod weblocks/dependencies:get-dependencies ((widget corpus-widget))
  (list (create-lass-dependency `((".corpus-widget-"
				   :min-height ,(format nil "calc(100vh - (~a + ~a + 0.75em))"
							(widget-height (header (parent widget)))
							(widget-height (footer (parent widget)))))))))

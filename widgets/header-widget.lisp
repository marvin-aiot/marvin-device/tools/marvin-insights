(in-package :marvin-web)

(defwidget header-widget ()
  ((parent :initarg :parent
	   :reader parent)
   (height :initarg :height
	   :initform "3em"
	   :reader widget-height)))

(defmethod get-css-classes ((widget header-widget))
  (append '(:navbar :navbar-light :bg-light :fixed-top :flex-md-nowrap :p-0 :shadow)
	  (call-next-method)))

(defmethod render ((widget header-widget))
  (with-html
    ;;(:nav :class "navbar navbar-dark fixed-top flex-md-nowrap p-0 shadow"
	  (:a :class "navbar-brand col-sm-3 col-md-2 mr-0" :href "#" "Marvin AIoT")
	  (:nav :class "my-2 my-md-0 mr-md-3"
		(:a :class "p-2 text-dark"
		    :href "#sign-in"
		    :onclick (make-js-action
			      (lambda (&key &allow-other-keys)
				(if (user-logged-in-p)
				    (action-logout)
				    (action-login))))
		    (if (user-logged-in-p)
			 "Sign out"
			 "Sign in"))
		(:a :class "p-2 text-dark"
		    :href "#help"
		    :onclick (make-js-action
			      (lambda (&key &allow-other-keys)
				(show-help)))
		    "Help"))));;)


(defmethod weblocks/dependencies:get-dependencies ((widget header-widget))
  (list (create-lass-dependency `((.navbar-brand
				   :font-size 1.4rem)))))
			    
(defun show-help ())
(defun action-login ())
(defun action-logout ())
(defun user-logged-in-p ())

(in-package :marvin-web)

(defwidget menu-widget ()
  ((parent :initarg :parent
	   :reader parent)))

(defmethod get-css-classes ((widget menu-widget))
  (append '(:col-md-2 :d-none :d-md-block :bg-light :sidebar)
	  (call-next-method)))

(defmethod render ((menu menu-widget))
  (with-html
    ;;(:nav :class "col-md-2 d-md-block bg-light sidebar"
	  (:div :class "sidebar-sticky"
		(:h6 :class "sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"
		     (:span "Platform"))
		(:ul :class "nav flex-column"
			  (iterate:iterate
			    (iterate:for entry in (platform-pages (parent menu)))
			    (:li :class "nav-item"
				 (:a :href (format nil "#~(~a~)" (route-tag entry))
				     :class "nav-link" ;; (concatenate 'string "ui-btn ui-corner-all" (if (page-selected entry) " ui-btn-active" ""))
				     :onclick (let ((entry entry)) ;; Make a localy bound copy
						(make-js-action
						 (lambda (&key &allow-other-keys)
						   (switch-to-page (parent menu) entry))))
				     (page-name entry)))))
		(:h6 :class "sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"
		     (:span "Plugins"))
		(:ul :class "nav flex-column"
		     (iterate:iterate
		       (iterate:for entry in (plugin-pages (parent menu)))
		       (:li :class "nav-item"
			    (:a :href (format nil "#~(~a~)" (route-tag entry))
				:class "nav-link" ;; (concatenate 'string "ui-btn ui-corner-all" (if (page-selected entry) " ui-btn-active" ""))
				:onclick (let ((entry entry)) ;; Make a localy bound copy
					   (make-js-action
					    (lambda (&key &allow-other-keys)
					      (switch-to-page (parent menu) entry))))
				(page-name entry))))))));)

(defmethod weblocks/dependencies:get-dependencies ((widget menu-widget))
  (list (create-lass-dependency '((.feather
				   :width 16px
				   :height 16px
				   :vertical-align text-bottom)

				  (.sidebar
				   :position fixed
				   :top 0
				   :bottom 0
				   :left 0
				   :z-index 100  ;; Behind the navbar 
				   :padding 48px 0 20px ;; Height of navbar (header and footer) 
				   :box-shadow inset -1px 0 0 (rgba 0 0 0 .1)


				   (.nav-link
				    :font-weight 500
				    :font-size 0.9em

				    (.feather
				     :margin-right 4px
				     :color "#999"))

				   (.nav-link.active
				    :color "#007bff")

				   ((:or (:and .nav-link :hover) (:and .nav-link .active))
				    (.feather
				     :color inherit)))

				  (.sidebar-sticky
				   :position relative
				   :top 0
				   :height "calc(100vh - 48px)"
				   :padding-top .5rem
				   :overflow-x hidden
				   :overflow-y auto) ;; Scrollable contents if viewport is shorter than content.

				  (:supports "((position: -webkit-sticky) or (position: sticky))"
				   (.sidebar-sticky
				    :position -webkit-sticky
				    :position sticky))

				  (.sidebar-heading
				   :font-size .75rem
				   :text-transform uppercase)))))


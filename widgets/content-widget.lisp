(in-package :marvin-web)

(defwidget content-widget ()
  ((parent :initarg :parent
	   :reader parent)))


(defmethod get-css-classes ((widget content-widget))
  (append '(:col-md-9 :ml-sm-auto :col-lg-10 :px-4)
	  (call-next-method)))

(defmethod render ((widget content-widget))
  (with-html
    (trivial-utilities:aif (selected-page (parent widget))
	(render it)
	"No page to show.")))


(defmethod weblocks/dependencies:get-dependencies ((widget content-widget))
  (list (create-lass-dependency '((.content-widget
				   :padding-top 133px) ;; /* Space for fixed navbar */
				  (:media "(min-width: 768px)"
				   (.content-widget
				   :padding-top 48px)) ;; /* Space for fixed navbar */
				  (.content-widget-disabled
				   :position relative
				   :float left
				   :padding-left 1em)
				  ("#contents"
				   :min-height 100%
				   :min-width "calc(100% - 13em)" ;; #(menu-min-width)
				   :position relative
				   :float left)))))

#|
[role="main"] {
  padding-top: 133px; /* Space for fixed navbar */
}

@media (min-width: 768px) {
  [role="main"] {
    padding-top: 48px; /* Space for fixed navbar */
  }
}
|#

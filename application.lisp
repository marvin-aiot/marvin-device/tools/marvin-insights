(in-package :marvin-web)

;;;;
;; See cljwt for JWT authentication
;;;;

(defapp marvin-web :prefix "/")
(defvar *application* nil)

(defwidget application ()
  ((header :initarg :header
	   :reader header)
   (corpus :initarg :corpus
	   :reader corpus)
   (footer :initarg :footer
	   :reader footer)
   (configuration :initarg :config
		  :initform '(:marvin/ip "127.0.0.1" :marvin/port 8081)
		  :accessor application-config)))

(defun get-config (key)
  (getf (application-config *application*) key))

(defmethod initialize-instance :after ((this application) &key)
  (setf (slot-value this 'header) (make-instance 'header-widget :parent this))
  (setf (slot-value this 'corpus) (make-instance 'corpus-widget :parent this))
  (setf (slot-value this 'footer) (make-instance 'footer-widget :parent this)))

(defmethod weblocks/dependencies:get-dependencies ((app application))
  (list
   (weblocks/dependencies:make-dependency "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css")
   (weblocks/dependencies:make-dependency "https://code.jquery.com/jquery-3.4.1.min.js")
   (weblocks/dependencies:make-dependency "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js")
   (weblocks/dependencies:make-dependency "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js")
   
   (weblocks/dependencies:make-dependency "http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css")
   (weblocks/dependencies:make-dependency "http://code.jquery.com/ui/1.12.1/jquery-ui.js")
   
   (weblocks/dependencies:make-dependency "https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.css")
   (weblocks/dependencies:make-dependency "https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js")))

(defmethod render ((app application))
  (with-html
    (:script :src "https://use.fontawesome.com/releases/v5.13.0/js/all.js" :data-auto-replace-svg "nest")
    (:script (comm-lib (get-config :marvin/ip) (get-config :marvin/port)))
    (render (header app))
    (render (corpus app))
    (render (footer app))))

;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-


(defsystem :marvin-web
  :name "marvin-web"
  :description ""
  :version "0.0.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "LLGPG"
  :depends-on (:weblocks
	       :weblocks-ui
	       :weblocks-file-server
	       :weblocks-lass
	       :parenscript
	       :water
	       :trivial-open-browser
	       :trivial-json-codec
	       :trivial-object-lock
	       :log4cl
	       :drakma)
  :components ((:file "package")
	       (:file "common")
	       (:file "common/weblocks-extension")
	       (:file "common/communication-support")
	       ;;(:file "common/draggable-widget-class")
	       ;;(:file "common/dropable-widget-class")
	       ;;(:file "common/resizable-widget-class")
	       (:file "widgets/header-widget")
	       (:file "widgets/footer-widget")
	       (:file "widgets/menu-widget")
	       (:file "widgets/content-widget")
	       (:file "widgets/corpus-widget")
	       (:file "application")
	       (:file "marvin-web")))

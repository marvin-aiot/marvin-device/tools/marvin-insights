(in-package :marvin-web/base-widget)

(export '(resizable-widget render-resizable resize-stopped widget-size))

(defclass resizable-widget ()
  ((size :initarg :size
	 :initform '(0 0)
	 :accessor widget-size)
   (on-resize-js :initarg :on-resize-js
	 :initform nil
	 :accessor on-resize-js)
   (on-resize-finished-js :initarg :on-resize-finished-js
	 :initform nil
	 :accessor on-resize-finished-js)))


(defun resize-stopped% (widget element)
  (assert (string-equal (getf element :id)
			(weblocks/widgets/dom:dom-id widget)))
  (let ((height (parse-integer (getf element :height) :junk-allowed t))
	(width (parse-integer (getf element :width) :junk-allowed t)))
    (setf (widget-size widget) (list height width))
    (resize-stopped widget height width)))

(defgeneric resize-stopped (widget height width))
(defmethod resize-stopped ((widget resizable-widget) height width))

(defun render-resizable (widget)
  (with-html
    (:script (ps (funcall (lambda ()
			    (let ((jq-elm ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id widget)))))
				  (elm (chain document (get-element-by-id (lisp (weblocks/widgets/dom:dom-id widget))))))
			      (setf (chain elm style height) (lisp (format nil "~dpx" (first (widget-size widget)))))
			      (setf (chain elm style width) (lisp (format nil "~dpx" (second (widget-size widget)))))
			      (chain jq-elm (resizable (create resize (lisp (if (on-resize-js widget)
										`(lambda (event ui)
										   (funcall (getprop window ,(lisp (on-resize-js widget))) event ui))
										nil))
							       stop (lambda (event ui)
								      (lisp (when (on-resize-finished-js widget)
									      `(funcall (getprop window ,(lisp (on-resize-finished-js widget))) event ui)))
								      (initiate-action-with-args (lisp (weblocks/actions:make-action
													(lambda (&rest rest)
													  (resize-stopped% widget (read-from-string
																   (symbol-name (car rest)))))))
												 nil
												 (+ "(:id \"" (chain elm id)
												    "\" :height \"" (chain elm style height)
												    "\" :width \"" (chain elm style width)
												    "\")"))
								      nil)))))
			    nil))))))

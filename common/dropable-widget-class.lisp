(in-package :marvin-web/base-widget)

(export '(dropable-widget element-dropped render-droppable))

(defclass dropable-widget ()
  ((accept :initarg :accept
	   :reader acceptable-draggables
	   :initform "*")
   (accepted-convert :initarg :accepted-convert
		     :initform (error "Every new dropped draggable must be converted to this class.")
		     :reader accepted-convert)
   (dragged-initialization :initarg :dragged-initialization
			   :initform nil
			   :reader dragged-initialization)))

(defun element-dropped% (widget element)
  (let ((classes (getf element :top))
	(top (parse-integer (getf element :top) :junk-allowed t))
        (left (parse-integer (getf element :left) :junk-allowed t)))
    (element-dropped widget classes top left)))

(defgeneric element-dropped (widget classes top left))
(defmethod element-dropped ((widget dropable-widget) classes top left)
  (log:warn "This method must be implemented for derived classes."))

(defun render-droppable (widget)
  (with-html
    (:script (ps (funcall (lambda ()
			    (chain ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id widget))))
				   (droppable (create accept "*"
						      drop (lambda (event ui)
							     (when (chain ui draggable (has-class (lisp (acceptable-draggables widget))))
							       (initiate-action-with-args (lisp (weblocks/actions:make-action
												 (lambda (&rest rest)
												   (element-dropped% widget (read-from-string
															    (symbol-name (car rest)))))))
											    nil
											    (+ "(:class \"" (chain ui helper 0 class-name)
											       "\" :top \"" (chain ui position top)
											       "\" :left \"" (chain ui position left)
											       "\")")))
							     nil))))
			    nil))))))


(in-package :marvin-web)

#|

This defines a JS library that encapsulates a WebSocket client and a HTTP client and exposes an unified API

Required use cases
- Request HTTP (uri [without protocol, ip and port part], on-complete, on-error/timeout)
-

|#


(defun comm-lib (marvin/ip marvin/port &key (enable-ssl nil))
  (ps
    (defun http-request (uri dom-context on-complete)
      (chain j-query (ajax (+ (lisp (format nil "~a://~a:~a/" (if enable-ssl "https" "http") marvin/ip marvin/port))  uri)
			   (create accepts "application/json"
				   context dom-context ;; The context can be accessed in handling function (on-complete) using $(this)
				   success (lambda (data text-status)
					     (chain console (log (+ "Request to " uri " was successful. Status: " text-status " and Data: " data)))
					     nil)
				   error (lambda (data)
					   (chain console (log (+ "Request to " uri " failed. Data: " data)))
					   (chain console (log data))
					   nil)
				   complete (lambda (jq-xhr text-status)
					      (funcall on-complete (chain jq-xhr response-J-S-O-N) (chain jq-xhr status))
					      nil))))
      nil)

     (defun make-data-subscription (on-message on-error on-open on-close)
      (new (data-subscription on-message on-error on-open on-close)))

     (defun ping (self))
       (chain self ws (send "ping")))

    (water:defwclass data-subscription
	(water:defwctor (on-message on-error on-open on-close)
	    (setf (chain this ready) nil)
	  (setf (chain this ws) (new (*web-socket (lisp (format nil "~a://~a:~a/api/v1/device-communication/messages" (if enable-ssl "wss" "ws") marvin/ip marvin/port)))))
	  (chain this ws (add-event-listener "error" (lambda (event)
						       (chain console (log (+ "Error message from server: " (chain event data))))
						       (chain console (log event))
						       (unless (equal on-error undefined)
							 (funcall on-error event)))))
	  (chain this ws (add-event-listener "open" (lambda (event)
						       (chain console (log (+ "Connection opened: " (chain event data))))
						       (chain console (log event))
						       (setf (chain this ready) t)
						       (unless (equal on-open undefined)
							 (funcall on-open event)))))
	  (chain this ws (add-event-listener "close" (lambda (event)
						       (chain console (log (+ "Connection closed: " (chain event data))))
						       (chain console (log event))
						       (setf (chain this ready) nil)
						       (unless (equal on-close undefined)
							 (funcall on-close event)))))
	  (chain this ws (add-event-listener "message" (lambda (event)
						       (chain console (log (+ "Message from server: " (chain event data))))
						       (chain console (log event))
						       (unless (equal on-message undefined)
							 (funcall on-message (chain *j-s-o-n (parse (chain event data))))))))

	  (setf (chain this timer-id) (set-interval (chain ping (bind this this)) 45000))
	  this)

      (water:defwmethod subscribe (device message)
	(chain this ws (send (+ "{\"action\" : \"subscribe\", \"device_id\" : " device ", \"message_id\" : " message "}")))
	nil)

      (water:defwmethod unsubscribe (device message)
	(chain this ws (send (+ "{\"action\" : \"unsubscribe\", \"device_id\" : " device ", \"message_id\" : " message "}")))
	nil)

      (water:defwmethod send (data)
	(if (equal (chain this ready) t)
	    (chain this ws (send data))
	    (chain console (log "Calling 'send' on an uninitialized connection!")))
	nil)

      (water:defwmethod close ()
	(clear-interval (chain this timer-id))
	;; @REVIEW This expects the server to handle all open subscriptions graciously
	(chain this ws (close))
	(setf (chain this ready) nil)
	nil))))


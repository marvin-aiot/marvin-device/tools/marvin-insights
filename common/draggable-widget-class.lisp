(in-package :marvin-web/base-widget)

(export '(draggable-widget render-draggable drag-stopped widget-position))

(defclass draggable-widget ()
  ((position :initarg :position
	     :initform '(0 0)
	     :accessor widget-position)
   (copy-on-drop :initarg :copy-on-drop
		 :initform nil
		 :type boolean
		 :accessor copy-on-drop)))

(defun drag-stopped% (widget element)
  (assert (string-equal (getf element :id)
			(weblocks/widgets/dom:dom-id widget)))
  (let ((top (parse-integer (getf element :top) :junk-allowed t))
        (left (parse-integer (getf element :left) :junk-allowed t)))
  ;; Update the (size widget) property
    (setf (widget-position widget) (list top left))
    (drag-stopped widget top left)))

(defgeneric drag-stopped (widget top left))
(defmethod drag-stopped ((widget draggable-widget) top left)
  (log:warn "This method must be implemented for derived classes."))

(defun render-draggable (widget)
  (with-html
    (:script (ps (funcall (lambda ()
			    (let ((jq-elm ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id widget)))))
				  (elm (chain document (get-element-by-id (lisp (weblocks/widgets/dom:dom-id widget))))))
			      (setf (chain elm style position) (lisp (if (copy-on-drop widget)
									 "relative"
									 "absolute")))
			      (setf (chain elm style top) (lisp (format nil "~dpx" (first (widget-position widget)))))
			      (setf (chain elm style left) (lisp (format nil "~dpx" (second (widget-position widget)))))
			      (chain jq-elm
				     (draggable (create grid (array 5 5)
							helper (lisp (if (copy-on-drop widget)
									 "clone"
									 "original"))
							containment (lisp (if (copy-on-drop widget)
									      "document"
									      "parent"))
							;;stack ".ui-droppable"
							stop (lambda (event ui)
							       ;;(chain console (log ui))
							       (initiate-action-with-args (lisp (weblocks/actions:make-action
												 (lambda (&rest rest)
												   (drag-stopped% widget (read-from-string
															  (symbol-name (car rest)))))))
											  nil
											  (+ "(:id \"" (chain elm id)
												    "\" :top \"" (chain elm style top)
												    "\" :left \"" (chain elm style left)
												    "\")"))
							       nil)))))
			    nil))))))

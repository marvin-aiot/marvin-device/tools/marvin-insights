(in-package :marvin-web)

(defun create-lass-dependency (lass-code)
  (make-instance 'weblocks-lass:lass-dependency
                 :type :css
                 :css (apply #'lass:compile-and-write lass-code)))

(defun serialize (data)
  (labels ((serialize$ (data stream)
		      (cond ((and (consp data)
				  (keywordp (car data)))
			     (let ((stream2 (list :guard 'parenscript:create)))
			       (iterate:iterate
				 (iterate:for elm on data by #'cddr)
				 (nconc stream2 (list (intern (symbol-name (first elm)))))
				 (serialize$ (second elm) stream2))
			       (assert (eq (car stream) :guard))
			       (nconc stream (list (cdr stream2)))))
			    ((consp data)
			     (let ((stream2 (list :guard 'parenscript:array)))
			       (iterate:iterate
				 (iterate:for elm in data)
				 (serialize$ elm stream2))
			       (assert (eq (car stream) :guard))
			       (nconc stream (list (cdr stream2)))))
			    (t (nconc stream (list data))))
		      (assert (eq (car stream) :guard))
		      (cdr stream)))
    (car (serialize$ data (list :guard)))))


(defclass dependency-holder ()
  ((js-dependencies :initform (list :before (make-hash-table) :after (make-hash-table))
                    :allocation :class
                    :reader get-js-dependencies)))

(defun define-dependency (widget position key ps-code)
    (declare (type (member :before :after) position)
	     (type keyword key)
	     (string ps-code))
    (trivial-object-lock:with-object-lock-held ((slot-value widget 'js-dependencies) :test #'equal)
      (setf (gethash key (getf (slot-value widget 'js-dependencies) position))
	    ps-code)))

(defgeneric inject-dependencies (position widget))
(defmethod inject-dependencies (position (widget dependency-holder))
  (declare (type (member :before :after) position))
  (let ((table (getf (slot-value widget 'js-dependencies) position)))
    (declare (type (or null hash-table) table))
    (unless table
      (return-from inject-dependencies nil))
    (with-html
      (iterate:iterate
	(iterate:for (key value) in-hashtable table)
	(:comment (format nil "JS Dependency: ~a" key))
	(weblocks/js:with-javascript
	    value)))))

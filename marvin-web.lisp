(in-package :marvin-web)


;;(weblocks/debug:on)
(defvar *port* 8080)

(defmethod weblocks/session:init ((app marvin-web))
  (declare (ignorable app))
  (setf *application* (make-instance 'application))
  (return-from weblocks/session:init *application*))

(defmethod render ((app marvin-web))
  "Default page rendering template and protocol"
  (with-html
    (render *application*)))

(defun serve-static-files ()
  (weblocks/server:serve-static-file "/assets/demo-line-chart.png"
				     (make-pathname
				      :name "demo-line-chart"
				      :type "png"
				      :directory (list :absolute
						       (namestring (asdf:system-source-directory :marvin-web))
						       "assets"))
				     :content-type "image/png"))
(defun start-app ()
  (weblocks/server:stop)

  (weblocks/hooks:on-application-hook-start-weblocks serve-static-files/start ()
						     (weblocks/hooks:call-next-hook)
						     (serve-static-files))

  (weblocks/hooks:on-application-hook-reset-session serve-static-files/reset (session)
						    (weblocks/hooks:call-next-hook)
						    (serve-static-files))
  
  (weblocks/server:start :port *port*)
  
  (trivial-open-browser:open-browser (format nil "http://127.0.0.1:~a/" *port*)))


(uiop:define-package #:marvin-web
    (:documentation "")
  (:use #:common-lisp
        #:weblocks-ui/form
        #:weblocks/html
	#:parenscript)
  (:import-from #:weblocks/widget
                #:render
                #:update
                #:defwidget
		#:get-css-classes)
  (:import-from #:weblocks/actions
                #:make-js-action)
  (:import-from #:weblocks/app
		#:defapp)
  (:export #:start-app
	   #:stop-app
	   #:page
	   #:add-page
	   #:create-lass-dependency
	   #:inject-dependencies
	   #:define-dependency
	   #:dependency-holder))


(uiop:define-package #:marvin-web/base-widget
    (:documentation "")
  (:use #:common-lisp
        #:weblocks-ui/form
        #:weblocks/html
	#:parenscript)
  (:import-from #:weblocks/widget
                #:render
                #:update
                #:defwidget
		#:get-css-classes)
  (:import-from #:weblocks/actions
                #:make-js-action)
  (:import-from #:marvin-web
		#:create-lass-dependency
		#:define-dependency))

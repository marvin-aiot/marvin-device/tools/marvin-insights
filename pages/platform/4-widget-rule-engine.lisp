(uiop:define-package #:marvin-web/rule-engine
    (:documentation "")
  (:use #:common-lisp
        #:weblocks-ui/form
        #:weblocks/html)
  (:import-from #:weblocks/widget
                #:render
                #:update
                #:defwidget
		#:get-css-classes)
  (:import-from #:weblocks/actions
                #:make-js-action)
  (:import-from #:marvin-web
                #:page
		#:create-lass-dependency
		#:add-page))

(in-package :marvin-web/rule-engine)


#|

Sub-widgets of this one:
re-navigation - Menu with elements ( Search | Create )
re-search-rule - Formular to search rules by Id, Name, Description and/or creation-time (from / to)
re-create-rule - Define Name and Description of the new Rule (opens re-edit-rule afterwards)
re-edit-rule - Visual editor of Rules via drag&drop (draw2d?) 
re-list-rules - Show a list of Rules (as returned from 'Search'); on click opens re-rule-details
re-rule-details - Show details of an existing Rule. Options/buttons for: edit, delete, enable/disable and close

|#

(defun show-page (page)
  )

(defwidget re-navigation ()
  ())

(defmethod get-css-classes ((widget re-navigation))
  (append '(:navbar :navbar-light :bg-light :flex-md-nowrap :px-4 :py-1 :no-shadow)
	  (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((page re-navigation))
  (list
   (create-lass-dependency '(((:and .navbar .widget .re-navigation)
			      :left -25px)))))

(defmethod render ((widget re-navigation))
  (with-html
    (:ul :class "nav nav-pills nav-fill"
	 (:li :class "nav-item"
	      (:a :class "nav-link"
		  :href "#"
		  :onclick (make-js-action
			    (lambda (&key &allow-other-keys)
			      (show-page :search)))
		  "Search Rules"))
	 (:li :class "nav-item"
	      (:a :class "nav-link"
		  :href "#"
		  :onclick (make-js-action
			    (lambda (&key &allow-other-keys)
			      (show-page :create)))
		  "Create new Rule")))))

(defwidget re-search-rule ()
  ())

(defwidget re-create-rule ()
  ())

(defwidget re-edit-rule ()
  ())


(defmethod get-css-classes ((widget re-edit-rule))
  (append '(:d-flex :flex-row)
	  (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((page re-edit-rule))
  (list
   (create-lass-dependency '((.draggable-item
			      :background red
			      :width 120px
			      :height 50px
			      :cursor move
			      :margin 10px)
			     (.re-drop-target
			      :height "calc(100vh - 120px)"
			      :border dashed 1px orange)))))

(defmethod render ((widget re-edit-rule))
  (with-html
    (:div :class "re-item-origin px-0 col-md-2"
     (:div :class "re-outside-drag-item draggable-item" "Drag me 1")
     (:div :class "re-outside-drag-item draggable-item" "Drag me 2")
     (:div :class "re-outside-drag-item draggable-item" "Drag me 3"))
    (:div :class "re-drop-target col-lg-9")
    (:script "(function () {
      $(\".re-drag-item\").draggable({
          grid: [5, 5]
      });
      $(\".re-outside-drag-item\").draggable({
          grid: [5, 5],
          helper:\"clone\"
      });
      $(\".re-drop-target\").droppable({
        accept: \"*\",
        drop: function(event, ui) {
                if (ui.draggable.hasClass('re-outside-drag-item')) {
                  
                  var obj = ui.draggable.clone()
                  $(this).append(obj);

                  obj.css('top', ui.position.top - $(this).position().top);
                  obj.css('left', ui.position.left - $(this).position().left);
                  obj.css('position', 'absolute');

                  obj.addClass('re-drag-item');
                  obj.removeClass('ui-draggable re-outside-drag-item'); 
                  obj.draggable({containment: 'parent'});
                }}
      });
    })();")))

(defwidget re-rule-details ()
  ())

(defwidget re-rule-details ()
  ())


;;----------------------------------------------------------------------
;;
;; The Widget itself
;;
;;----------------------------------------------------------------------


(defwidget rule-engine-page (page)
  ((menu :initarg :menu
	 :initform nil
	 :reader rule-engine-menu)
   (editor :initarg :editor
	   :initform nil
	   :reader rule-engine-editor)))

(defmethod initialize-instance :after ((this rule-engine-page) &key)
  (setf (slot-value this 'menu) (make-instance 're-navigation))
  (setf (slot-value this 'editor) (make-instance 're-edit-rule)))


(defmethod weblocks/dependencies:get-dependencies ((page rule-engine-page))
  (list
   ;;"js/draw2d/dist/draw2d.js"

   #+disabled
   (create-lass-dependency '())))

(defmethod render ((page rule-engine-page))
  (with-html
    ;; Render a top Navigation menu ( Search | Create )
    (render (rule-engine-menu page))
    (render (rule-engine-editor page))))

(add-page (make-instance 'rule-engine-page :name "Rule Engine" :tag :rules))

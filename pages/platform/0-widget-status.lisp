(in-package :marvin-web)

(defwidget status-page (page)
  ())

(defmethod render ((widget status-page))
  (with-html
    (:p "This shows the overall status of Marvin AIoT.")))


(marvin-web:add-page (make-instance 'status-page :name "Status" :tag :status))

(uiop:define-package #:marvin-web/device-communication
    (:documentation "")
  (:use #:common-lisp
        #:weblocks-ui/form
        #:weblocks/html
	#:parenscript)
  (:import-from #:weblocks/widget
                #:render
                #:update
                #:defwidget
		#:get-css-classes)
  (:import-from #:weblocks/actions
                #:make-js-action)
  (:import-from #:marvin-web
                #:page
		#:create-lass-dependency
		#:add-page
		#:serialize
		#:inject-dependencies
		#:define-dependency
		#:dependency-holder))

(in-package :marvin-web/device-communication)


#|

# Concept of this page

## Structure

### Menu
A horizontal menu located at the top of the page. Contains following elements:
Devices (ignored for now)
- Discover
- Commission

Dashboards
- List Dashboards (left aligned, list icon)
- Create new Dashboard (left aligned, plus icon) - add a new entry to (session storage, dashboards) and open 'edit details'
- Every existing dashboard (right aligned) - listens to the data (session storage, dashboards)

### Page body
Shows one of many contents, depending on the selection from the menu.
Implemented now is edit-dashboard


#### Widget edit-dashboard
On initialization all data about the configured elements of the dashboard is loaded as session data
twice: the original and the editable.

Whenever something is changed (e.g. widget position or size) the dashboard is marked as 'modified'.
Modified dashboard have a different font in the menu, a star '*' appears on the name and a save and
an reset icon are also shown in the menu.
When saving, the original data is overwritten by the editable one and this data is sent to the backend.
When reverting, the editable data is overwritten by the original and the dashboard is redrawn.

#### Widget list-dashboards
Show a table with all dashboards found in the dashboards directory.
Columns are:
- Name
- Description
- External URL
- Creation data/time
- Modification date/time
- Actions
-- Edit Details
-- Clone - add a copy of this entry to (session storage, dashboards) and open 'edit details'
-- Disable
-- Delete


#### Base widget for Dashboard elements

Each Dashboard element comes in two flavors: static (image as placeholder) & dynamic (live data)

Properties
is-dynamic - Used when rendering to select visualization and dependencies
on-drop-js - JS method to call when this is dropped (converts from static to dynamic)


Methods
render - as Weblocks:render
collect-js-dependencies 
  *type* - (member (:before :after)) - Select position to insert the script in HTML
  *key*  - keyword - Unique identifier to avoid duplicate in JS
  *ps-body* - list - Parenscript code


JS 'Static' class 

Properties

Methods
on-drop - 


Before Dependencies

After Dependencies



JS 'Dynamic' class

Properties

Methods

Before Dependencies

After Dependencies


|#









(defgeneric remove-widget (this widget))

(defun list-dashboards ()
  (iterate:iterate
    (iterate:for file in (directory (make-pathname :name :wild
						   :type "lisp"
						   :directory (list :absolute
								    (namestring (asdf:system-source-directory :marvin-web))
								    "dashboards"))))
    (iterate:collect (pathname-name file))))


;; ------------------------------------------------------------------------------

(let ((files '("base-dashboard-element"
	       "page-menu"
	       "chart-widget"
	       "dashboard-canvas")))
  (iterate:iterate
   (iterate:for file in files)
   (load (make-pathname :name file
			:type "lisp"
			:directory (list :absolute
					 (namestring (asdf:system-source-directory :marvin-web))
					 "pages" "plugins" "device-communication-private" ))
	 :print t :verbose t)))

;; ------------------------------------------------------------------------------

(defwidget avaliable-charts-container ()
  ;; 
  ((avaliable-charts :initarg :avaliable-charts
		     :initform nil
		     :reader avaliable-charts)))

(defmethod get-css-classes ((widget avaliable-charts-container))
  (append '()
	  (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((widget avaliable-charts-container))
  (list
   (create-lass-dependency '((.avaliable-charts-container
			     :border-top "ghostwhite 3px ridge"
			     :bottom "calc(1px - 100vh + (190px + 48px + 20px + 44px))")))))

(defmethod render ((widget avaliable-charts-container))
  (with-html
    (iterate:iterate
      (iterate:for chart in (avaliable-charts widget))
      (render chart))))

;; ------------------------------------------------------------------------------

;;----------------------------------------------------------------------
;;
;; The Widget itself
;;
;;----------------------------------------------------------------------

(defwidget device-communication-page (page)
  ((menu :initarg :menu
	 :initform nil
	 :reader device-communication-menu)
   (canvas :initarg :canvas
	   :initform nil
	   :reader canvas)
   (placeholder-container :initarg :container
			  :initform nil
			  :reader placeholder-container)))

(defmethod initialize-instance :after ((this device-communication-page) &key)
  (setf (slot-value this 'menu) (make-instance 'dc-navigation))
  (setf (slot-value this 'canvas) (make-instance 'dashboard-canvas))

  ;; @TODO How to make this more dynamic/configrable?
  (let ((avaliable-charts (list (make-instance 'chart-widget
					       :dynamic nil
					       :static-image-url "assets/demo-line-chart.png"
					       :js-create-fn (ps:symbol-to-js-string :replace-dropped-chart)))))
    (setf (slot-value this 'placeholder-container) (make-instance 'avaliable-charts-container :avaliable-charts nil))))

(defmethod render ((widget device-communication-page))
  (let ((tmp (make-instance 'dependency-holder)))
    (with-html
      (inject-dependencies :before tmp)
      
      (render (device-communication-menu widget))
      (render (canvas widget))
      (render (placeholder-container widget))

      (inject-dependencies :after tmp)

      (weblocks/js:with-javascript (ps (insert-widget "static-chart-widget"
						      ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id (placeholder-container widget)))))
						      (create position (create top 100
									       left 100))))))))

(add-page (make-instance 'device-communication-page :name "Device Communication" :tag :device-communication))


(in-package :marvin-web/device-communication)

(defwidget sensor-data-widget (marvin-web/base-widget:draggable-widget
			       marvin-web/base-widget:resizable-widget
			       weblocks/widgets/base:widget)
  ;; jQuery UI Draggable snap to grid
  ;; Contains a ChartJs object and a title
  ;; Is configurable to select the devices/messages to show (more than one possible) - Popup on click
  ;; A data/value transformation hook is avaliable
  ((parent :initarg :parent
	   :initform (error "A parent widget must be informed.")
	   :reader parent)
   (title :initarg :title
	  :initform "Unnamed Chart"
	  :reader widget-title)
   (sources :initarg :sources
	    :initform nil
	    :accessor widget-sources)
   (parser-fn :initarg :parser-fn
	      :initform #'identity
	      :accessor parser-fn)
   (convert-fn :initarg :convert-fn
	       :initform #'identity
	       :accessor convert-fn)
   (chart-data :initarg :chart-data
	       :initform nil
	       :reader chart-data)
   (chart-options :initarg :chart-options
		  :initform nil
		  :reader chart-options))
  (:default-initargs
      :on-resize-finished-js (symbol-to-js-string :keep-resizing-aspect)))

(defmethod render ((widget sensor-data-widget))
  (with-html
    ;; The configuration button
    (:div.widget-config
     (:a :href "#"
	 (:i :class "fas fa-sliders-h" :data-toggle "modal"
	     :data-target (format nil "#~a-modal" (weblocks/widgets/dom:dom-id widget))))
     (:a :href "#"
	 :onclick (ps (funcall (lambda ()
				 (let ((result (confirm "Should this widget be removed?")))
				   (when result
				     (initiate-action (lisp (weblocks/actions:make-action
							     (lambda (&key &allow-other-keys)
							       (remove-widget (parent widget) widget)))))))
				 nil)))
	 (:i :class "far fa-trash-alt red-icon")))

    (weblocks/js:with-javascript
	(ps (defun generate-table-head (table data)
	      (let* ((thead (chain table (create-t-head)))
		     (row (chain thead (insert-row -1))))
		(setf (chain thead class-name) "thead-dark")
		(for-in (key data)
			(let ((th (chain document (create-element "th")))
			      (text (chain document (create-text-node (getprop data key)))))
			  (chain th (append-child text))
			  (chain row (append-child th)))))
	      nil)

	    (defun generate-table (table data)
	      (let ((tbody (chain table (create-t-body))))
		(for-in (element data)
			(let ((row (chain tbody (insert-row -1)))
			      (row-data (getprop data element)))
			  (for-in (key row-data)
				  (let ((cell (chain row (insert-cell)))
					(text (chain document (create-text-node (getprop row-data key)))))
				    (chain cell (append-child text))))
			  (let* ((a (chain (chain row (insert-cell)) (append-child (chain document (create-element "a")))))
				 (i (chain a (append-child (chain document (create-element "i"))))))
			    (setf (chain a href) "#")
			    (setf (chain a onclick) (lambda ()
						      (chain row (remove)))) ;; @REVIEW Seems this could lead to memory leaks. Must also remove elemenet in row recursively.
			    (setf (chain i class-name) "far fa-trash-alt red-icon")))))
	      nil)

	    (defun check-input (input min-value max-value act)
	      (let ((is-valid nil))
		(let ((val (*number (chain input value))))
		  (unless (or (eq (chain input value length) 0) ;; Number("") gives 0 so we must check also for empty
			      (is-na-n val)
			      (< val min-value)
			      (< max-value val))
		    (setf is-valid t)))

		(when act
		  (if (or is-valid (eq (chain input value length) 0))
		      (progn
			(chain input style (remove-property "color")))
		      (progn
			(chain input style (set-property "color" "red"))
			;; @TODO Show popover
			)))

		is-valid))

	    (defun draw-sources-table (table sources)
	      (funcall generate-table-head table (array "Device Id" "Message Id" "Options"))
	      (generate-table table sources)
	      (let* ((row (chain table (insert-row -1)))
		     (dev-input (chain (chain row (insert-cell)) (append-child (chain document (create-element "input")))))
		     (msg-input (chain (chain row (insert-cell)) (append-child (chain document (create-element "input")))))
		     (button (chain (chain row (insert-cell)) (append-child (chain document (create-element "a")))))
		     (icon (chain button (append-child (chain document (create-element "i"))))))
		(setf (chain dev-input :type) "text")
		(setf (chain dev-input :maxlength) 3)
		(setf (chain dev-input :size) 14)
		(setf (chain dev-input :placeholder) "New Device Id")
		(setf (chain dev-input :onkeyup) (lambda (event)
						   (if (and (check-input dev-input 0 255 t)
							    (check-input msg-input 0 2047 nil))
						       (chain button class-list (remove "disabled-button"))
						       (chain button class-list (add "disabled-button")))))

		(setf (chain msg-input :type) "text")
		(setf (chain msg-input :maxlength) 3)
		(setf (chain msg-input :size) 14)
		(setf (chain msg-input :placeholder) "New Message Id")
		(setf (chain msg-input :onkeyup) (lambda (event)
						   (if (and (check-input dev-input 0 255 nil)
							    (check-input msg-input 0 2047 t))
						       (chain button class-list (remove "disabled-button"))
						       (chain button class-list (add "disabled-button")))))

		(setf (chain button href) "#")
		(setf (chain button class-name) "disabled-button")
		(setf (chain button onclick) (lambda (event)
					       (let* ((new-row (chain table (insert-row (max 0 (- (chain table rows length) 1))))))
						 (let ((dev (chain new-row (insert-cell)))
						       (dev-text (chain document (create-text-node (chain dev-input value)))))
						   (chain dev (append-child dev-text)))
						 (let ((msg (chain new-row (insert-cell)))
						       (msg-text (chain document (create-text-node (chain msg-input value)))))
						   (chain msg (append-child msg-text)))

						 (let* ((a (chain (chain new-row (insert-cell)) (append-child (chain document (create-element "a")))))
							(i (chain a (append-child (chain document (create-element "i"))))))
						   (setf (chain a href) "#")
						   (setf (chain a onclick) (lambda ()
									     (chain new-row (remove)))) ;; @REVIEW Seems this could lead to memory leaks. Must also remove elemenet in row recursively.
						   (setf (chain i class-name) "far fa-trash-alt red-icon")))))
		(setf (chain icon class-name) "far fa-plus-square"))
	      nil)))

    ;; The configuration dialog
    (:div :class "modal fade" :id (format nil "~a-modal" (weblocks/widgets/dom:dom-id widget))
	  :data-backdrop "static" :tabindex "-1" :role "dialog" :aria-hidden "true"
	  :aria-labelledby (format nil "~a-modal-label" (weblocks/widgets/dom:dom-id widget))
	  (:div :class "modal-dialog" :role "document"
		(:div :class "modal-content"
		      (:div :class "modal-header"
			    (:h5 :class "modal-title" :id (format nil "~a-modal-label" (weblocks/widgets/dom:dom-id widget)) "Widget Configuration"))
		      (:div :class "modal-body"
			    (:table :class "table" :id (format nil "~a-sources-table" (weblocks/widgets/dom:dom-id widget))
				    ))
		      (:div :class "modal-footer"
			    (:button :type "button" :class "btn btn-secondary" :data-dismiss "modal" "Cancel")
			    (:button :type "button" :class "btn btn-primary" :data-dismiss "modal" "Save")))))

    (:canvas)
    (marvin-web/base-widget:render-draggable widget)
    (marvin-web/base-widget:render-resizable widget)

    (:style (lass:compile-and-write '(.widget-config
				      :display none
				      :position absolute
				      :top 0px
				      :right 0px)
				    `((:and ,(format nil "#~a" (weblocks/widgets/dom:dom-id widget))
					    .dc-widget-hover)
				      (.widget-config
				       :display block
				       :z-index 10))
				    '(.disabled-button
				      :color grey
				      :opacity 0.5
				      :pointer-events none
				      :cursor not-allowed)
				    '(.red-icon
				      :color red)))

    (weblocks/js:with-javascript
	(ps (funcall (lambda ()
		       (let* ((widget (chain document (get-element-by-id (lisp (weblocks/widgets/dom:dom-id widget)))))
			      (canvas ($ (chain widget (get-elements-by-tag-name "canvas") 0)))
			      (overlay ($ (chain widget (get-elements-by-class-name "widget-config") 0))))
			 (chain ($ canvas)
				(hover
				 (lambda ()
				   (chain ($ widget) (add-class "dc-widget-hover")))
				 (lambda ()
				   (chain ($ widget) (remove-class "dc-widget-hover")))))
			 (chain ($ overlay)
				(hover
				 (lambda ()
				   (chain ($ widget) (add-class "dc-widget-hover")))
				 (lambda ()
				   (chain ($ widget) (remove-class "dc-widget-hover"))))))
		       nil))))

    ;; @REVIEW This script could/should exist only once. Add a method {append|prepend}-html-script
    (weblocks/js:with-javascript
	(ps (defun :keep-resizing-aspect (event ui)
	      (let* ((widget (chain ($ (chain ui helper 0)) 0))
		     (canvas (chain widget (get-elements-by-tag-name "canvas") 0)))
		(setf (chain widget style height) (chain canvas style height))
		(setf (chain widget style width) (chain canvas style width))))))

    (weblocks/js:with-javascript
	(ps (create-chart-widget
	     (lisp (weblocks/widgets/dom:dom-id widget))
	     :line ;; @TODO This must depend on the class of the dropped widget
	     (lisp (serialize (widget-sources widget)))
	     (lisp (serialize (chart-data widget)))
	     (lisp (serialize (chart-options widget))))))

    (weblocks/js:with-javascript
	(ps (draw-sources-table (chain document (get-element-by-id (lisp (format nil "~a-sources-table" (weblocks/widgets/dom:dom-id widget)))))
				(chain ($ (lisp (concatenate 'string "#" (weblocks/widgets/dom:dom-id widget))))
				       (data :widget-data) sources))))))


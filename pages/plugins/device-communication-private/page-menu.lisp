
(in-package :marvin-web/device-communication)



(defun show-page (page)
  (declare (ignore page)))

(defwidget dc-navigation ()
  ())

(defmethod get-css-classes ((widget dc-navigation))
  (append '(:navbar :navbar-light :bg-light :flex-md-nowrap :px-4 :py-1 :no-shadow)
	  (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((page dc-navigation))
  (list
   (create-lass-dependency '(((:and .navbar .widget .dc-navigation)
			      :left -25px)))))

(defmethod render ((widget dc-navigation))
  (with-html
    (:ul :class "nav nav-tabs nav-fill"
	 (:li :class "nav-item"
	      (:a :class "nav-link"
		  :href "#"
		  :onclick (make-js-action
			    (lambda (&key &allow-other-keys)
			      (show-page :list)))
		  "List Dashboards"))
	 (:li :class "nav-item"
	      (:a :class "nav-link active"
		  :href "#"
		  :onclick (make-js-action
			    (lambda (&key &allow-other-keys)
			      (show-page :create)))
		  "Create new Dashboard")))))



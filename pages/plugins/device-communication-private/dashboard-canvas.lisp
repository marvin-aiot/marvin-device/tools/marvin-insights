
(in-package :marvin-web/device-communication)

(defwidget dashboard-canvas (marvin-web:dependency-holder
			  weblocks/widgets/base:widget)
  ((charts :initarg :charts
	   :initform nil
	   :reader charts)
   (dashboard :initarg :dashboard
	      :initform "Default"
	      :reader dashboard-name)))


(defmethod initialize-instance :after ((this dashboard-canvas) &key)
    (marvin-web:define-dependency
	this :after :save-widgets
	(ps
	  (defun collect-widgets ()
	    (loop
	       for widget in ($ (lisp (format nil ".~(~a~)" :dynamic-dashboard-widget)))
	       collect (create widget-class (chain ($ widget) (data :widget-class))

			       ;; @TODO From the widget-data all chart data (labels, datasets & autosettings)  must be removed
			       widget-data (chain ($ widget) (data :widget-data)))))))

    (marvin-web:define-dependency
	this :after :add-droppable-hook
	(ps
	  (chain *Array (from ($ ".dashboard-canvas"))
		 (for-each
		  (lambda (element)
		    (chain ($ element)
			   (droppable (create accept ".draggable-widget"
					      drop
					      (lambda (event ui)
						;; Check if this is already the parent
						(unless (chain ($ this) (is ($ (chain ui draggable 0 parent-node))))
						  ;;(chain console (log ui))
						  ;;(chain console (log event))
						  (drop-widget ($ this) ui))

						nil))))
		    nil)))))

    (marvin-web:define-dependency
	this :after :add-dashboard-hoover-events
	(ps
	  (let ((hoover-elm ($ "#dashboard-hoover")))
	    (if hoover-elm
		(chain hoover-elm
		       (hover
			(lambda ()
			  (chain hoover-elm (add-class "dashboard-hovering")))
			(lambda ()
			  (chain hoover-elm (remove-class "dashboard-hovering")))))
		(chain console (log "Some element is missing ('#dashboard-hoover')"))))

	  (chain ($ "#save-dashboard")
		 (on "click"
		     (lambda ()
		       (let ((widgets (collect-widgets)))
			 (initiate-action-with-args (lisp (weblocks/actions:make-action
							   (lambda (&rest rest)
							     (save-widgets (symbol-name (car rest))))))
						    nil
						    (chain *j-s-o-n (stringify widgets))))))))))

(defmethod get-css-classes ((widget dashboard-canvas))
  (append '(:w-100)
	  (call-next-method)))

(defmethod weblocks/dependencies:get-dependencies ((widget dashboard-canvas))
  (list
   (create-lass-dependency '((.dashboard-canvas
			      :height "calc(100vh - (100px + 48px + 20px + 44px))")
			     (.widget-hoover
			      :display none
			      :position absolute
			      :top 0px
			      :right 0px)
			     (.dc-widget-hover
			      (.widget-hoover
			       :display block
			       :z-index 10))
			     ("#dashboard-hoover"
			      :height 2rem)
			     (.dashboard-hovering
			      ("#dashboard-buttons"
			       :display block
			       :z-index 10))
			     ("#dashboard-buttons"
			       :display none
			       :z-index -1)
			     (.disabled-button
			      :color grey
			      :opacity 0.5
			      :pointer-events none
			      :cursor not-allowed)
			     (.red-icon
			      :color red)
			     (.blue-icon
			      :color blue)))))

(defmethod render ((widget dashboard-canvas))
  (with-html
    (:div :id "dashboard-hoover"
	 (:div :id "dashboard-buttons" :class "mx-auto w-25"
	       (:a :id "save-dashboard" :class "mr-4" :href "#"
		   (:i :class "far fa-save blue-icon"))
	       (:a :id "delete-dashboard"  :class "ml-4" :href "#"
		   (:i :class "far fa-trash-alt red-icon"))))))

(defun save-widgets (data)
  (with-open-file (stream
		   (ensure-directories-exist
		    (make-pathname :name "Default"
				   :type "lisp"
				   :directory (list :absolute
						    (namestring (asdf:system-source-directory :marvin-web))
						    "dashboards")))
		    :direction :output :if-exists :overwrite :if-does-not-exist :create)
    (write-string
     (write-to-string
      (trivial-json-codec:deserialize-raw data))
     stream)))

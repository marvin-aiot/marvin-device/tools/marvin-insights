
(in-package :marvin-web/device-communication)

(defclass dashboard-widget (marvin-web:dependency-holder)
  ((dynamic :initarg :dynamic
	    :initform nil
	    :reader dynamicp
	    :type boolean)
   (js-create-fn :initarg :js-create-fn
		 :initform nil
		 :type string
		 :reader js-create-fn)
   (on-resize-js :initarg :on-resize-js
	 :initform nil
	 :accessor on-resize-js)
   (on-resize-finished-js :initarg :on-resize-finished-js
	 :initform nil
	 :accessor on-resize-finished-js)
   (static-image-url :initarg :static-image-url
		     :initform nil
		     :reader static-image-url)))

;;#+disabled
(defmethod initialize-instance :after ((this dashboard-widget) &key)
  #+disabled
  (define-dependency
   this :before :add-draggable-hook
   (ps (let ((observer (new (*mutation-observer 
			     (lambda (mutations) 
			       (chain mutations
				      (for-each
				       (lambda (mutation)
					 (if (and (chain mutation added-nodes) 
						  (> (chain mutation added-nodes length) 0))
					     ;; element added to DOM
					     (progn
					       (chain console (log (chain mutation added-nodes)))
					       (chain mutation added-nodes
						      (for-each (lambda (el)
								  (when (and (chain el class-list) (chain el class-list (contains "draggable-widget")))
								    ;; element has class `draggable-widget`
								    (chain console (log "Element '.draggable-widget' added"))
								    (chain console (log mutation))
								    (make-draggable ))
								  nil))))
					     nil))))))))

	     (config (create
		      attributes t
		      attribute-filter (*array "class")
		      child-list t
		      subtree t
		      character-data nil)))

	 (chain console (log "Registering 'draggable' observer."))
	 (chain observer (observe (getprop (chain document (get-elements-by-class-name "device-communication-page")) 0)  config)))

       (defun make-draggable (widget top left is-static create-widget-fn)
	 ;; Split this function into two: one gets called on .static-dashboard-widget and the other on .dynamic-dashboard-widget
	 (setf (chain widget style position) (if is-static
						 "relative"
						 "absolute"))
	 (setf (chain widget style top) top)
	 (setf (chain widget style left) left)
	 (chain ($ widget)
		(draggable (create grid (array 5 5)
				   scroll t
				   helper (if is-static
					      "clone"
					      "original")
				   containment (if is-static
						   "document"
						   "parent")
				   ;;stack ".ui-droppable"
				   stop ;; @TODO This should be used to switch from static to dynamic widgets
				   ;; or to update the position of the widget in session storage (for dynamic widgets)
				   (lambda (event ui)
				     (chain console (log ui))
				     (funcall (getprop window create-widget-fn)
					      ui (create auto-settings t))
				     nil))))
	 nil)))

  (define-dependency
      this :before :widget-creation-functions
      (ps
	(defvar widget-creator-map (create))

	(defun register-widget-creator (klass fn)
	  (setf (getprop widget-creator-map klass) fn))

	(defun insert-widget (klass target options)
	  (let ((creator nil))
	    (loop for key in (chain *object (keys widget-creator-map))
	       do (when (chain klass (includes key))
		    (setf creator (getprop widget-creator-map key))
		    (break)))
	    (if creator
		(funcall creator target options)
		(chain console (log (+ "No widget creator known for class '" klass "'."))))))

	(defun drop-widget (target ui)
	  (let ((classes (or (chain ui helper 0 class-name) ""))
		(creator nil))
	    (loop for key in (chain *object (keys widget-creator-map))
	       do (when (chain classes (includes key))
		    (setf creator (getprop widget-creator-map key))
		    (break)))
	    (if creator
		(let ((options (chain (chain ui helper) (data :widget-creation))))
		  (setf (chain options position) (create top (chain ($ (chain ui helper 0)) (css "top"))
							 left (chain ($ (chain ui helper 0)) (css "left"))))
		  (funcall creator target options))
		(chain console (log (+ "No widget creator known for class '" klass "'."))))))))

  (define-dependency
      this :before :widget-sources-table
      (ps
	(defun generate-table-head (table data)
	  (let* ((thead (chain table (create-t-head)))
		 (row (chain thead (insert-row -1))))
            (setf (chain thead class-name) "thead-dark")
            (for-in (key data)
                    (let ((th (chain document (create-element "th")))
			  (text (chain document (create-text-node (getprop data key)))))
                      (chain th (append-child text))
                      (chain row (append-child th)))))
	  nil)

	(defun generate-table (table data)
	  (let ((tbody (chain table (create-t-body))))
	    (for-in (element data)
		    (let* ((row-data (getprop data element))
			   (dev-text (getprop row-data 0))
			   (msg-text (getprop row-data 1))
			   (label-text (getprop row-data 2)))
		      (fill-row table tbody dev-text msg-text label-text -1))))
	  nil)

	(defun fill-row (table tbody dev-text msg-text label-text row-position)
	  (let ((new-row (chain tbody (insert-row row-position))))
	    (let ((dev (chain new-row (insert-cell)))
		  (dev-text (chain document (create-text-node dev-text))))
	      (chain dev (append-child dev-text)))
	    (let ((msg (chain new-row (insert-cell)))
		  (msg-text (chain document (create-text-node msg-text))))
	      (chain msg (append-child msg-text)))
	    (let ((label (chain new-row (insert-cell)))
		  (label-text (chain document (create-text-node label-text))))
	      (chain label (append-child label-text)))

	    (let* ((a (chain (chain new-row (insert-cell)) (append-child (chain document (create-element "a")))))
		   (i (chain a (append-child (chain document (create-element "i"))))))
	      (setf (chain a href) "#")
	      (setf (chain a onclick) (lambda ()
					(let* ((data (chain ($ table) (data :sources))))
					  (do ((i (- (chain data length) 1) (decf i)))
					      ((< i 0))
					    (let* ((dev-v dev-text)
						   (msg-v msg-text)
						   (label-v label-text)
						   (dev (getprop data i 0))
						   (msg (getprop data i 1))
						   (label (getprop data i 2)))
					      (if (and (eq dev-v dev)
						       (eq msg-v msg)
						       (eq label-v label))
						  (chain data (splice i 1)))))
					  (chain ($ table) (data :sources nil))
					  (chain ($ table) (data :sources data)))
					(chain new-row (remove)) ;; @REVIEW Seems this could lead to memory leaks. Must also remove elemenet in row recursively.
					nil))
	      (setf (chain i class-name) "far fa-trash-alt red-icon")))
	  nil)

	(defun check-input (input min-value max-value act)
	  (let ((is-valid nil))
	    (let ((val (*number (chain input value))))
	      (unless (or (eq (chain input value length) 0) ;; Number("") gives 0 so we must check also for empty
			  (is-na-n val)
			  (< val min-value)
			  (< max-value val))
		(setf is-valid t)))

	    (when act
	      (if (or is-valid (eq (chain input value length) 0))
		  (progn
		    (chain input style (remove-property "color")))
		  (progn
		    (chain input style (set-property "color" "red"))
		    ;; @TODO Show popover
		    )))

	    is-valid))
	
	(defun draw-sources-table (table sources)
	  ;; Clear the table
	  (let* ((table-jq ($ table))
		 (thead (chain table-jq (children "thead")))
		(tbody (chain table-jq (children "tbody"))))
	    (when thead (chain thead (remove)))
	    (when tbody (chain tbody (remove))))

	  (generate-table-head table (array "Device Id" "Message Id" "Label" "Options"))
	  (generate-table table sources)
	  (chain ($ table) (data :sources (chain sources (map (lambda (x) x)))))

	  (let* ((row (chain ($ table) (find "tbody") 0 (insert-row -1)))
		 (dev-input (chain (chain row (insert-cell)) (append-child (chain document (create-element "input")))))
		 (msg-input (chain (chain row (insert-cell)) (append-child (chain document (create-element "input")))))
		 (label-input (chain (chain row (insert-cell)) (append-child (chain document (create-element "input")))))
		 (button (chain (chain row (insert-cell)) (append-child (chain document (create-element "a")))))
		 (icon (chain button (append-child (chain document (create-element "i"))))))
	    (setf (chain dev-input :type) "text")
	    (setf (chain dev-input :maxlength) 3)
	    (setf (chain dev-input :size) 10)
	    (setf (chain dev-input :placeholder) "New Device Id")
	    (setf (chain dev-input :onkeyup) (lambda (event)
					       (if (and (check-input dev-input 0 255 t)
							(check-input msg-input 0 2047 nil))
						   (chain button class-list (remove "disabled-button"))
						   (chain button class-list (add "disabled-button")))))

	    (setf (chain msg-input :type) "text")
	    (setf (chain msg-input :size) 10)
	    (setf (chain msg-input :placeholder) "New Message Id")
	    (setf (chain msg-input :onkeyup) (lambda (event)
					       (if (and (check-input msg-input 0 2047 t)
							(check-input dev-input 0 255 nil))
						   (chain button class-list (remove "disabled-button"))
						   (chain button class-list (add "disabled-button")))))

	    (setf (chain label-input :type) "text")
	    (setf (chain label-input :maxlength) 3)
	    (setf (chain label-input :size) 10)
	    (setf (chain label-input :placeholder) "Label")

	    (setf (chain button href) "#")
	    (setf (chain button class-name) "disabled-button")
	    (setf (chain button onclick) (lambda (event)
					   (fill-row table
						     (chain ($ table) (find "tbody") 0)
						     (chain dev-input value)
						     (chain msg-input value)
						     (chain label-input value)
						     (max 0 (- (chain ($ table) (find "tbody") 0 rows length) 1)))

					   (chain ($ table) (data :sources)
						  (push (new (*array (chain dev-input value)
								     (chain msg-input value)
								     (chain label-input value)))))

					   (setf (chain dev-input value) "")
					   (setf (chain msg-input value) "")
					   (setf (chain label-input value) "")
					   (setf (chain button class-name) "disabled-button")
					   (chain dev-input (focus))
					   nil))
	    (setf (chain icon class-name) "fas fa-plus-square"))
	  nil)))

  (define-dependency
      this :after :add-resizable-hook
      (ps
	(let ((observer (new (*mutation-observer 
			      (lambda (mutations) 
				(chain mutations
				       (for-each
					(lambda (mutation)
					  (if (and (chain mutation added-nodes) 
						   (> (chain mutation added-nodes length) 0))
					      ;; element added to DOM
					      (let ((has-class (chain (new *array)
								      some
								      (call (chain mutation added-nodes) 
									    (lambda (el)
									      (chain el class-list (contains "resizable-widget")))))))
						(when has-class
						  (let ((widget ($ (chain mutation added-nodes 0))))
						    (if (chain widget (data :widget-data))
							(let ((data (chain widget (data :widget-data))))
							  (when (chain data size)
							    (chain widget (css "height" (chain data size height)))
							    (chain widget (css "width" (chain data size width))))

							  (when (chain widget (data :widget-data) resizable)
							    (let ((resizable-data (chain widget (data :widget-data) resizable)))
							      (chain widget
								     (resizable (create resize
											(lambda (event ui)
											  (when (chain resizable-data resizing-fn)
											    (funcall (chain resizable-data resizing-fn) event ui))
											  nil)
											stop
											(lambda (event ui)
											  (when (chain resizable-data resize-finished-fn)
											    (funcall (chain resizable-data resize-finished-fn) event ui))
											  nil)))))))
							(progn
							  (chain console (log "The widget has no data."))
							  nil)))))))))))))

	      (config (create
		       attributes nil
		       child-list t
		       character-data nil)))

	  (if (getprop (chain document (get-elements-by-class-name "dashboard-canvas")) 0)
	      (chain observer (observe (getprop (chain document (get-elements-by-class-name "dashboard-canvas")) 0)  config))
	      (chain console (error "In 'add-resizable-hook': Element 'dashboard-canvas' was not found!")))
	  nil)))

  #+disabled
  (define-dependency
   this :after :dashboard-widget-hover-listener
   (ps
     (chain console (log "Running one-time scripts."))
     ;; Add hover listeners to each widget
     (funcall
      (lambda ()
	(chain *Array (from (chain document (get-elements-by-class-name "chart-widget")))
	       (for-each
		(lambda (element)
		  (let* ((widget element)
			 (canvas ($ (chain widget (get-elements-by-tag-name "canvas") 0)))
			 (overlay ($ (chain widget (get-elements-by-class-name "widget-config") 0))))
		    (chain ($ canvas)
			   (hover
			    (lambda ()
			      (chain ($ widget) (add-class "dc-widget-hover")))
			    (lambda ()
			      (chain ($ widget) (remove-class "dc-widget-hover")))))
		    (chain ($ overlay)
			   (hover
			    (lambda ()
			      (chain ($ widget) (add-class "dc-widget-hover")))
			    (lambda ()
			      (chain ($ widget) (remove-class "dc-widget-hover"))))))
		  nil)))))))

  ;; Add listeners to the 'close' and 'save' buttons of modal settings dialogs
  #+disabled
  (define-dependency
   this :after :settings-button-click-listener
   (ps
     (funcall
      (lambda ()
	(let ((save-buttons (chain document (get-elements-by-class-name "widget-modal-save-button"))))
	  (chain *Array (from save-buttons)
		 (for-each
		  (lambda (element)
		    (chain element (add-event-listener "click"
						       (lambda(event)
							 (chain event (prevent-default))
							 (chain console (log event))
							 ;; Save all changes
							 (chain ($ (chain element dataset modal-id))
								(modal "hide"))
							 nil)))
		    nil))))
	(let ((cancel-buttons (chain document (get-elements-by-class-name "widget-modal-cancel-button"))))
	  (chain *Array (from cancel-buttons)
		 (for-each
		  (lambda (element)
		    (chain element (add-event-listener "click"
						       (lambda(event)
							 (chain event (prevent-default))
							 (chain console (log event))
							 ;; Revert all changes
							 (chain ($ (chain element dataset modal-id))
								(modal "hide"))
							 nil)))
		    nil))))
	nil)))))

(defmethod get-css-classes ((widget dashboard-widget))
  (append '(:draggable-widget)
	  (if (dynamicp widget)
	      '(:dynamic-dashboard-widget :resizable-widget)
	      '(:static-dashboard-widget))
	  (call-next-method)))



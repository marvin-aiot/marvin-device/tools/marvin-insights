
(in-package :marvin-web/device-communication)


(defwidget chart-widget (dashboard-widget weblocks/widgets/base:widget)
  ())

(defmethod initialize-instance :after ((this chart-widget) &key)
  (define-dependency
      this :before :chart-creation
    (ps
      (defun create-dynamic-chart-widget (target options)
	(let* ((classes "widget dynamic-dashboard-widget chart-widget dynamic-chart-widget resizable-widget draggable-widget")
	       (id (chain *math (random) (to-string 36) (substring 8)))
	       (style (+ "position: absolute;top: " (chain options position top) ";left: " (chain options position left) ";"
			 "width: " (chain options size width) ";height: " (chain options size height) ";")))
	  ($ (chain target
		    (append (+ "<div id='" id "' class='" classes "' style='" style "' data-widget-class='dynamic-chart-widget'>"
			       "<div class='widget-hoover'></div>"
			       "<div class='widget-settings'></div>"
			       "<canvas></canvas></div>"))))

	  (let ((widget ($ (+ "#" id))))
	    (chain widget (data :widget-data
				(create type :line
					auto-settings (chain options auto-settings)
					labels (new (*array))
					datasets (new (*array ))
					options (create scales (create x-axes (new (*array (create display t
												   scale-label (create display t
														       label-string "Date"))))
								       y-axes (new (*array (create display t
												   scale-label (create display t
														       label-string "Degree Centigrade (°C)")
												   ticks (create beginatzero t)))))
							title (create display t
								      text "Temperature")
							tooltips (create mode "index"
									 intersect nil)
							hoover (create mode "nearest"
								       intersect t))
					sources (new (*array))
					position (create top (chain options position top)
							 left (chain options position left))
					size (create height (chain options size height)
						     width (chain options size width))
					subscription (create helper (make-data-subscription (lambda (data) (chart-add-data widget (new (*date)) data)))
							     sources (new (*array)))
					resizable (create resize-finished-fn (getprop window (lisp (ps:symbol-to-js-string 'keep-resizing-aspect)))))))

	    (chain widget (draggable (create grid (array 5 5)
					     scroll t
					     helper "original"
					     containment "parent"
					     stop (lambda (event ui)
						    (setf (chain widget (data :widget-data) position top)
							  (+ (chain *math (round (chain ui position top))) "px"))
						    (setf (chain widget (data :widget-data) position left)
							  (+ (chain *math (round (chain ui position left))) "px"))))))

	    (add-modal-settings widget)
	    (add-widget-hoover-buttons widget)
	    (make-chart widget)))
	nil)

      (defun create-static-chart-widget (target options)
	(let* ((classes "widget static-dashboard-widget chart-widget static-chart-widget resizable-widget draggable-widget")
	       (id (chain *math (random) (to-string 36) (substring 8)))
	       (img-src "assets/demo-line-chart.png")
	       (style "position: relative;"))
	  ($ (chain target
		    (append (+ "<div id='" id "' class='" classes "' style='" style "' data-widget-class='static-dashboard-widget'><img src='" img-src "'/></div>"))))
	  (chain ($ (+ "#" id)) (draggable (create grid (array 5 5)
						   ;;scroll t
						   helper
						   (lambda ()
						     (let ((drag (chain ($ this) (clone))))
						       (chain drag (remove-class "static-dashboard-widget"))
						       (chain drag (remove-class "static-chart-widget"))
						       (chain drag (add-class "dynamic-dashboard-widget"))
						       (chain drag (add-class "dynamic-chart-widget"))
						       (chain drag (data :widget-creation (create size (create width "200px"
													       height "100px")
												  auto-settings t)))
						       drag))

						   containment "document")))
	  id))

      (defun make-chart (widget)
	(let* ((canvas (chain widget (children "canvas") 0)))
  	  (setf (chain canvas the-chart)
  		(new (*chart (chain canvas (get-context :2d))
  			     (create type (chain widget (data :widget-data) type)
  				     data (chain widget (data :widget-data) data)
  				     options (chain widget (data :widget-data) options)))))
  	  (unless (chain canvas the-chart options on-click)
  	    (setf (chain canvas the-chart options on-click)
  		  (lambda (event rest)
  		    (chain console (log "I've been clicked"))
  		    (chain console (log event))
  		    (chain console (log rest))))))
	nil)

      (defun delete-widget-event (widget-id)
	(when (confirm "Should this widget be removed?")
	  (let ((widget ($ (+ "#" widget-id))))
	    (chain console (log widget))
	    (chain widget (remove))))
	nil)

      (defun add-widget-hoover-buttons (widget)
	(let* ((overlay (chain widget (children "div.widget-hoover")))
	       (canvas (chain widget (children "canvas"))))
	  ($ (chain overlay
		    (append (+ " <a href=# data-toggle=\"modal\" data-target=\"#" (chain widget (attr "id")) "-modal\"><i class=\"fas fa-sliders-h\"></i></a>
 <a href=#
 onclick=\"" (lisp (ps:symbol-to-js-string :delete-widget-event)) "('" (chain widget (attr "id")) "');\"><i class=\"far fa-trash-alt red-icon\"></i></a>"))))

	  (chain canvas
		 (hover
		  (lambda ()
		    (chain widget (add-class "dc-widget-hover")))
		  (lambda ()
		    (chain widget (remove-class "dc-widget-hover")))))
	  (chain overlay
		 (hover
		  (lambda ()
		    (chain widget (add-class "dc-widget-hover")))
		  (lambda ()
		    (chain widget (remove-class "dc-widget-hover")))))))

      (defun add-modal-settings (widget)
	(let ((id (chain widget (attr "id")))
	      (target (chain widget (children "div.widget-settings"))))

	  (chain target
		 (replace-with (+ "<div class=\"modal fade widget-settings\" id=" id "-modal
     data-backdrop=static tabindex=-1
     role=dialog aria-hidden=true
     aria-labelledby=" id "-modal-label>
 <div class=\"modal-dialog modal-lg\"
      role=document>
  <div class=modal-content>
   <div class=modal-header>
    <h5 class=modal-title id=" id "-modal-label>Widget Configuration</h5>
   </div>
   <div class=modal-body>
     <div class=\"input-group mb-3\">
       <div class=\"input-group-prepend\">
         <span class=\"input-group-text\" id=\"widget-" id "-title-label\">Title</span>
       </div>
       <input type=\"text\" id=\"widget-" id "-title\" class=\"form-control\" placeholder=\"Give me a title\" aria-label=\"Username\" aria-describedby=\"widget-" id "-title-label\">
     </div>
     <div class=\"input-group mb-3\">
       <div class=\"input-group-prepend\">
         <span class=\"input-group-text\" id=\"widget-" id "-xaxis-label\">X-Axis Description</span>
       </div>
       <input type=\"text\" id=\"widget-" id "-xaxis\" class=\"form-control\" placeholder=\"Name the horizontal axes\" aria-label=\"Username\" aria-describedby=\"widget-" id "-xaxis-label\">
     </div>
     <div class=\"input-group mb-3\">
       <div class=\"input-group-prepend\">
         <span class=\"input-group-text\" id=\"widget-" id "-yaxis-label\">Y-Axis Description</span>
       </div>
       <input type=\"text\" id=\"widget-" id "-yaxis\" class=\"form-control\" placeholder=\"Name the vertical axes\" aria-label=\"Username\" aria-describedby=\"widget-" id "-yaxis-label\">
     </div>
     <table class=table id=" id "-sources-table></table>
   </div>
   <div class=modal-footer>
    <button
            class=\"btn btn-secondary widget-modal-cancel-button\"
            type=button data-modal-id=#" id "-modal>Cancel</button>
    <button
            class=\"btn btn-primary widget-modal-save-button\"
            type=button data-modal-id=#" id "-modal>Save</button>
   </div>
  </div>
 </div>
</div>")))

	  (chain ($ (+ "#" id "-modal"))
		 (on "show.bs.modal"
		     (lambda (event)
		       (draw-sources-table (chain document (get-element-by-id (+ id "-sources-table")))
	      				   (chain widget (data :widget-data) sources)))))

	  (let ((save-buttons (chain widget (find "button.widget-modal-save-button"))))
	    (chain *Array (from save-buttons)
		   (for-each
		    (lambda (element)
		      (chain element (add-event-listener "click"
							 (lambda(event)
							   (chain event (prevent-default))

							   ;; Clear subscriptions
							   (chain widget (data :widget-data) sources
								  (for-each
								   (lambda (source)
								     (chart-unsubscribe widget
											(getprop source 0)
											(getprop source 1)
											(getprop source 2))
								     nil)))

							   ;; Save all changes
							   (setf (chain widget (data :widget-data) sources) nil)
							   (setf (chain widget (data :widget-data) sources)
								 (chain ($ (+ "#" id "-sources-table")) (data :sources)
									(map (lambda (x) x))))

							   (let ((title (chain document (get-element-by-id (+ "widget-" id "-title")) value))
								 (xaxis (chain document (get-element-by-id (+ "widget-" id "-xaxis")) value))
								 (yaxis (chain document (get-element-by-id (+ "widget-" id "-yaxis")) value))
								 (data-scales (chain widget (data :widget-data) options scales))
								 (chart-scales (chain widget (children "canvas") 0 the-chart options scales)))
							     (setf (chain widget (data :widget-data) options title text) title)
							     (setf (chain widget (children "canvas") 0 the-chart options title text) title)

							     (setf (chain data-scales x-axes 0 scale-label label-string) xaxis)
							     (setf (chain data-scales y-axes 0 scale-label label-string) yaxis)
							     (setf (chain chart-scales x-axes 0 scale-label label-string) xaxis)
							     (setf (chain chart-scales y-axes 0 scale-label label-string) yaxis))

							   ;;Subscribe to each sources
							   (chain widget (data :widget-data) sources
								  (for-each
								   (lambda (source)
								     (chart-subscribe widget
										      (getprop source 0)
										      (getprop source 1)
										      (getprop source 2))
								     nil)))


							   (chain widget (children "canvas") 0 the-chart (update))

							   (chain ($ (chain element dataset modal-id))
								  (modal "hide"))
							   nil)))
		      nil))))
	  (let ((cancel-buttons (chain widget (find "button.widget-modal-cancel-button"))))
	    (chain *Array (from cancel-buttons)
		   (for-each
		    (lambda (element)
		      (chain element (add-event-listener "click"
							 (lambda(event)
							   (chain event (prevent-default))
							   ;; Revert all changes
							   (let ((table ($ (+ "#" id "-sources-table"))))
							     (chain table (data :sources nil))
							     (chain table (data :sources
										(chain widget (data :widget-data) sources
										       (map (lambda (x) x))))))
							   (chain ($ (chain element dataset modal-id))
								  (modal "hide"))
							   nil)))
		      nil))))

	  (let ((show-settings (chain widget (data :widget-data) auto-settings)))
	    (when show-settings
	      (setf (chain widget (data :widget-data) auto-settings) nil)
	      (chain ($ (+ "#" id "-modal"))
		     (modal "show"))))))))

  (define-dependency
      this :after :chart-data
    (ps
      (defun chart-subscribe (widget dev-id msg-id set-label)
	(let* ((chart (chain widget (children "canvas") 0 the-chart)))
	  ;; Create a new dataset
	  (chain chart data datasets (push (create label set-label
						   data (new (*array))
						   fill nil)))
	  (chain widget (data :widget-data) subscription helper (subscribe dev-id msg-id))
	  (chain widget (data :widget-data) subscription sources (push (new (*array dev-id msg-id set-label)))))
	nil)

      (defun chart-unsubscribe (widget dev-id msg-id set-label)
	(let* ((chart (chain widget (children "canvas") 0 the-chart)))
	  (chain widget (data :widget-data) subscription helper (unsubscribe dev-id msg-id))
	  ;; @TODO Remove the dataset
	  )
	nil)

      ;; @TODO The timestamp of the data must come from Marvin!
      (defun chart-add-data (widget date data)
	(let ((chart (chain widget (children "canvas") 0 the-chart))
	      (label (chain date (to-locale-string)))
	      (dev-id (chain data device_id))
	      (msg-id (chain data message_id)))
	  (chain chart data labels (push label))
	  (chain chart data datasets
		 (for-each
		  (lambda (dataset) ;; @REVIEW This!
		    (chain widget (data :widget-data) subscription sources
			   (for-each
			    (lambda (source)
			      (when (and (equal (getprop source 0) dev-id)
					 (equal (getprop source 1) msg-id)
					 (equal (getprop source 2) (chain dataset label)))
  				(chain dataset data
				       ;; @REVIEW It would be better to insert {X Y} point, but for now we use value only
				       (push (chain data value))))
			      nil)))
		    nil)))

	  ;;(when (> (chain length) 100)
	  ;; @TODO Decide when to pop labels and data.
	  (chain chart (update)))
	nil)))

  (define-dependency
      this :before :keep-resizing-aspect
    (ps (defun keep-resizing-aspect (event ui)
	  (let* ((widget (chain ($ (chain ui helper 0)) 0))
		 (canvas (chain widget (get-elements-by-tag-name "canvas") 0)))
            (setf (chain widget style height) (chain canvas style height))
            (setf (chain widget style width) (chain canvas style width))
	    (setf (chain ($ widget) (data :widget-data) size height) (chain canvas style height))
	    (setf (chain ($ widget) (data :widget-data) size width) (chain canvas style width)))
	  nil)))

  (define-dependency
      this :after :chart-creation
    (ps (register-widget-creator "static-chart-widget" create-static-chart-widget)
	(register-widget-creator "dynamic-chart-widget" create-dynamic-chart-widget))))

;; @REVIEW A widget has to be defined for this to work! It must be moved to a dependency.
(defmethod weblocks/dependencies:get-dependencies ((widget chart-widget))
  (list
   (create-lass-dependency
    `((.widget-config
       :display none
       :position absolute
       :top 0px
       :right 0px)
      (;;(:and ,(format nil "#~a" (weblocks/widgets/dom:dom-id widget))
       .dc-widget-hover;;)
       (.widget-config
	:display block
	:z-index 10))
      (.disabled-button
       :color grey
       :opacity 0.5
       :pointer-events none
       :cursor not-allowd)
      (.red-icon
       :color red)))))

